# spyro-fix

Fixes the issue where Spyro Reignited plays cutscenes in random languages under Proton

As of GE-Proton 7-24, the random language issue still seems to exist, though cutscenes seem to play properly in standard Proton now. If you still need/want to use GE, this fix can help you.
The workaround is to create copies of the cutscene files with only the audio track in the desired language. But since there are 30 video files to modify mixed in with 47 other videos you have to sort through, and one might have to edit them again later after a reinstall or if Steam ever "repairs" the files, this script will make them for you automatically

#Instructions

1, Confirm that you have the "ffmpeg" package installed

2, Download spyro-fix.sh

3, If your game is not installed in the default Steam location you will have to edit the targetdir and backupdir paths in the script

4, Optional: You can switch to any of the other included languages by editing the "language" number in the script (to get the number for another language, open Movies/C_Spyro01_Intro.mp4 with any media player and find the audio track you want. The index number you have to put in the script will be one lower than the the audiotrack's number). Or if someone wants to take the time to identify them all, then I will include a comment in the script with the list

5, Make the file executable with "chmod +x ./spyro-fix.sh"

6, Run spyro-fix.sh


If you want to restore everything to the original conditions afterwards without any downloading required, simply delete the modified "Movies" folder and rename "Movies_backup" to "Movies"